void main(){

  int j              = 0;
  int ciclos         = 2000;
  int nivel          = 0;
  String ascendiente = null;

  ListaDobleEnlace listaNodos = new ListaDobleEnlace();

  // Elemento inicial y la solucion deseada
  Elemento escenarioInicial = new Elemento(estado: "4531 2678", solucion: "12345678 ", nivel: 0, padre: null);
  
  // Agrega elemento inicial a la lista 
  listaNodos.agrega(escenarioInicial);
  listaNodos.obtieneMejor();

  while(j < ciclos && listaNodos.mejor.dato.noSolucion()){
      nivel       = listaNodos.mejor.dato.nivel;
      ascendiente = listaNodos.mejor.dato.padre;
      // Genera los descendientes del mejor nodo
      // Se incrementa el nivel de profundidad 
      for(var hijo in listaNodos.mejor.dato.descendientes(nivel + 1)){
        // No mete a la lista el descendiente igual al ascendiente del mejor nodo
        if(ascendiente != hijo.estado){
          listaNodos.agrega(hijo);
        }  
      } 
      // Vuelve a obtener mejor nodo
      listaNodos.obtieneMejor();   
      j = j + 1;
  }

  // Recupera lista de mejores nodos de abajo hacia arriba
  // pero se imprime al reves
  print("======================================");
  for(var e in listaNodos.recuperaElementos().reversed){
    e.imprime("nivel y heuristica");
  }
}

// Elemento (Puzzle)
class Elemento{
  String   estado        = null;
  String   solucion      = null;
  String   padre         = null;
  int      heuristica    = 0;
  int      nivel         = 0;
  int      explorado     = 0;

  Elemento({ this.estado, this.solucion, this.nivel, this.padre }){
    this.heuristica = distanciaManhatan();
    this.explorado  = 0;
  }

  // Calcula funcion de eveluacion del elemento
  int funcion(){
    return heuristica + nivel;
  }

  // Calculo la heuristica del elemento
  int distanciaManhatan(){
    int j    = 0;
    int dmx  = 0;
    int dmy  = 0;
    int suma = 0;
    String    pieza = "12345678";
    List<int> coorx = [1,1,1,2,2,2,3,3,3];
    List<int> coory = [1,2,3,1,2,3,1,2,3];
    for(j=0; j<8; j++) {
      dmx  = coorx[estado.indexOf(pieza[j])] - coorx[solucion.indexOf(pieza[j])];
      dmy  = coory[estado.indexOf(pieza[j])] - coory[solucion.indexOf(pieza[j])];
      suma = suma + dmx.abs() + dmy.abs();
    }
    return suma;
  }

  // Genera descendientes del elemento
  List<Elemento> descendientes(int nivel){
    List<List<int>>  mvtosValidos = [[1,3,9,9], [0,2,4,9],   [1,5,9,9],
                                     [0,4,6,9], [1,3,5,7,9], [2,4,8,9],
                                     [3,7,9,9], [4,6,8,9],   [5,7,9,9]];

    int    vacio       = estado.indexOf(" ");
    int    j           = 0; 
    int    p           = 0;
    String auxiliar    = null;
    List<Elemento> lst = new List<Elemento>();

    // En base al lugar del espacio y los movimientos validos hace los intercambios de piezas
    while(mvtosValidos[vacio][j] != 9){
      p = mvtosValidos[vacio][j];
      auxiliar = estado.replaceRange(vacio, vacio + 1, estado[p]);
      auxiliar = auxiliar.replaceRange(p, p + 1, " ");
      j = j + 1;
      Elemento d = new Elemento(estado: auxiliar, solucion: solucion, nivel: nivel, padre: estado);
      lst.add(d);
    }
    return lst;
  }

  // Muestra el estado de cada elemento y sus valores de evaluacion
  void imprime(String msg){
    String lin1 = estado.substring(0,3);
    String lin2 = estado.substring(3,6);
    String lin3 = estado.substring(6,9);
    print("${lin1} ->  ${msg} -> ${nivel} -> ${heuristica}");
    print("${lin2} ->                   ");
    print("${lin3} ->                   ");
    print("________");
  }

  // El elemento es solucion ?
  bool noSolucion(){
    if(estado != solucion) return true;
    else return false;
  }
}

// Nodo a usar en la lista 
class Nodo{
  Elemento dato;
  var      previo;
  var      siguiente;
  Nodo({this.dato, this.previo, this.siguiente});
}

// Lista de elementos (Puzzle's)
class ListaDobleEnlace{
  Nodo cabeza    = null;
  Nodo cola      = null;
  Nodo mejor     = null;
  int  explorado = 1;

  // Agrega elemento a la lista 
  void agrega(dato){
    Nodo nuevo = new Nodo(dato: dato, previo: null, siguiente: null);
    if(cabeza == null){
      // Para nodo inicial
      cabeza    = nuevo;
      cola      = nuevo;
      mejor     = nuevo;
    } else {
      // Para nodos subsecuentes
      nuevo.previo    = mejor;
      nuevo.siguiente = null;
      cola.siguiente  = nuevo;
      cola            = nuevo;
    }
  }

  // Recupera elementos de la lista de abajo hacia arriba
  // Del ultimo mejor nodo hacia sus ascendientes
  List<Elemento> recuperaElementos(){
    Nodo actual        = mejor;
    List<Elemento> lst = new List<Elemento>();
    while(actual != null){
      lst.add(actual.dato);
      actual = actual.previo;
    }
    return lst;
  }

  // Apunta al mejor nodo de la lista calificado por sus funciones de evaluacion
  void obtieneMejor(){
    int funcion          = 0;
    int maximo           = 2000;
    int heuristica       = 0;
    int maximaHeuristica = maximo;

    // Ciclo que obtiene el mejor valor de la funcion
    Nodo i = cabeza;
    while(i != null){
      funcion = i.dato.funcion();
      // Solo evalua nodos no explorados 
      // (No mejores hasta ese momento)
      if(i.dato.explorado == 0 && funcion < maximo){
        maximo = funcion;
      }
      i = i.siguiente;
    }  

    // Ciclo que obtiene el mejor nodo (mejor funcion y menor heuristica)
    i = cabeza;
    while(i != null){
      funcion    = i.dato.funcion();
      heuristica = i.dato.heuristica;
      // Solo evalua nodos no explorados 
      if(i.dato.explorado == 0 && funcion == maximo && heuristica < maximaHeuristica ){
        mejor            = i;
        maximaHeuristica = heuristica;
      }
      i = i.siguiente;
    }
    // Al final del ciclo queda apuntador al mejor nodo
    explorado = explorado + 1;
    // Se etiqueta como explorado
    mejor.dato.explorado = explorado;
  }
}

# Dart

Ejercicios de programación con DART

# Lista Doblemente enlazada (listadoble.dart)

https://es.wikipedia.org/wiki/Lista_doblemente_enlazada

Adicionalmente se emplea el concepto de funcion generadora, en Dart pueden ser de 2 tipos: 

* Asincronas que regresan un Stream
* Sincronas que regresan un Iterable

"Yield" es una palabra reservada para regresar un valor de la sequencia sin parar la ejecucion de la funcion.

# Rompecabezas 8 piezas (rompecabezas.dart)

Esta programado en base al algoritmo A* (https://es.wikipedia.org/wiki/Algoritmo_de_búsqueda_A*). 

Es un programa de busqueda IA que da solucion al rompecabezas de 8 piezas. El algoritmo es informado, emplea una funcion heuristica que indica que tan bueno es un estado en relacion a su semejenza con el estado solucion.

El archivo PNG muestra la imagen del comportamiento del programa:

* CABEZA apunta al primer elemento de la lista. 
* COLA siempre apunta al elemento final. 
* MEJOR apunta al mejor nodo sin explorar. 
* MEJOR va cambiando en cada ciclo, se determina en base a al valor de la heuristica "h" de cada nodo y su nivel.
* En base a MEJOR se generan los nuevos elementos o descendientes que se van agregando a la lista.
* Cada nodo tiene dos enlaces, "siguiente" apunta al siguiente elemento de la lista y "previo" apunta al nodo padre.

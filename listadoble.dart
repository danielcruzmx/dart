
void main(){

  ListaDobleEnlace listaNodos = new ListaDobleEnlace();

  String uno  = "uno";
  String dos  = "dos";
  String tres = "tres";

  listaNodos.agrega(uno);
  listaNodos.agrega(dos);
  listaNodos.agrega(tres);

  Iterable<String> iniFin = listaNodos.recupera();
  for(String d in iniFin)print(d);

  Iterable<String> finIni = listaNodos.reversa();
  for(String d in finIni)print(d);
}

// Nodo a usar en la lista 
class Nodo{
  String   dato;
  var      previo;
  var      siguiente;
  Nodo({this.dato, this.previo, this.siguiente});
}

// Lista doble enlace 
class ListaDobleEnlace{
  Nodo cabeza    = null;
  Nodo cola      = null;

  // Agrega nodo a la lista 
  void agrega(dato){
    Nodo nuevo = new Nodo(dato: dato, previo: null, siguiente: null);
    if(cabeza == null){
      // Para nodo inicial
      cabeza    = nuevo;
      cola      = nuevo;
    } else {
      // Para nodos subsecuentes
      nuevo.previo    = cola;
      nuevo.siguiente = null;
      cola.siguiente  = nuevo;
      cola            = nuevo;
    }
  }

  // Recupera nodos cabeza->cola
  Iterable<String> recupera() sync* {
    Nodo actual = cabeza;
    while(actual != null){
      yield actual.dato;
      actual = actual.siguiente;
    }
  }

  // Recupera nodos cola->cabeza
  Iterable<String> reversa() sync* {
    Nodo actual = cola;
    while(actual != null){
      yield actual.dato;
      actual = actual.previo;
    }
  }
}
